<?php

/** @var yii\web\View $this */

$this->title = 'Gestion de productos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestion de Articulos y Categorias</h1>

        <p class="lead">Aplicacion para realizar la gestion de mis productos y de las categorias</p>
    </div>

    <?= \yii\helpers\Html::img("@web/imgs/articulos.jpg") ?>
    
</div>
